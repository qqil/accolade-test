<?php
namespace App\TestTask1\Contracts;


interface GeneratorContract
{
    public function getValues(): iterable;
}
