<?php
namespace App\TestTask1;

use App\TestTask1\Contracts\GeneratorContract;
use App\TestTask1\Helpers\FileReader;
use App\TestTask1\Helpers\Hasher;

class Bruteforcer
{
    /**
     * @var GeneratorContract
     */
    protected $passwordGenerator;

    /**
     * @var GeneratorContract
     */
    protected $saltGenerator;

    /**
     * @var string
     */
    protected $passwordsFilePath;

    /**
     * TaskSolver constructor.
     * @param GeneratorContract $passwordGenerator
     * @param GeneratorContract $saltGenerator
     * @param string $passwordsFilePath
     */
    public function __construct(
        GeneratorContract $passwordGenerator,
        GeneratorContract $saltGenerator,
        string $passwordsFilePath
    )
    {
        $this->passwordGenerator = $passwordGenerator;
        $this->saltGenerator = $saltGenerator;
        $this->passwordsFilePath = $passwordsFilePath;
    }

    /**
     * @return iterable
     */
    public function execute(): iterable
    {
        foreach ($this->getPasswordsToDecode() as $passwordHash) {
            $counter = 0;
            $timeStarted = $this->getMicrotime();

            foreach ($this->passwordGenerator->getValues() as $password) {
                foreach ($this->saltGenerator->getValues() as $salt) {
                    $counter++;

                    if (Hasher::compare($passwordHash, $salt . '$' . $password)) {
                        $timeFinished = $this->getMicrotime();

                        yield [
                            'password' => $password,
                            'hash' => $passwordHash,
                            'salt' => $salt,
                            'count' => $counter,
                            'seconds' => round(($timeFinished - $timeStarted),2)
                        ];
                    }
                }
            }
        }


        return;
    }

    /**
     * @return iterable
     */
    protected function getPasswordsToDecode(): iterable
    {
        yield from FileReader::read($this->passwordsFilePath);
    }

    /**
     * @return float
     */
    protected function getMicrotime()
    {
        list($usec, $sec) = explode(" ", microtime());

        return ((float)$usec + (float)$sec);
    }
}
