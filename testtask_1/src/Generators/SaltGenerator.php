<?php
namespace App\TestTask1\Generators;


use App\TestTask1\Contracts\GeneratorContract;

class SaltGenerator implements GeneratorContract
{
    /**
     * @var int
     */
    protected $saltLength;

    /**
     * @var array
     */
    protected $saltCharSet;

    /**
     * SaltGenerator constructor.
     * @param int $saltLength
     * @param array $saltCharSet
     */
    public function __construct(int $saltLength, array $saltCharSet)
    {
        $this->saltLength = $saltLength;
        $this->saltCharSet = $saltCharSet;
    }

    /**
     * @return iterable
     */
    public function getValues(): iterable
    {
        yield from $this->generate();
    }

    /**
     * @param int $index
     * @param array $result
     * @return \Generator
     */
    protected function generate(int $index = 0, array $result = [])
    {
        if ($index === $this->saltLength) {
           yield implode("", $result);
           return;
        }

        foreach ($this->saltCharSet as $char) {
            $result[$index] = $char;
            yield from $this->generate($index + 1, $result);
        }
    }
}
