<?php
namespace App\TestTask1\Generators;

use App\TestTask1\Contracts\GeneratorContract;
use App\TestTask1\Exceptions\FileException;
use App\TestTask1\Helpers\FileReader;

class PasswordGenerator implements GeneratorContract
{
    /**
     * @var string
     */
    protected $passwordDictionaryPath;

    /**
     * PasswordGenerator constructor.
     * @param null $passwordDictionaryPath
     */
    public function __construct($passwordDictionaryPath = null)
    {
        $this->passwordDictionaryPath = $passwordDictionaryPath;
    }

    /**
     * @return iterable
     */
    public function getValues(): iterable
    {
        yield from FileReader::read($this->passwordDictionaryPath);
    }
}
