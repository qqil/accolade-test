<?php
namespace App\TestTask1;

use App\TestTask1\Exceptions\FileException;
use App\TestTask1\Generators\PasswordGenerator;
use App\TestTask1\Generators\SaltGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ConsoleCommand extends Command
{
    /**
     * Configure console command
     */
    protected function configure()
    {
        $this->setName('password-bruteforce')
            ->setDescription('Resolve testtask_1')
            ->setDefinition(
                new InputDefinition([
                    new InputOption(
                        'dictionary',
                        'd',
                        InputOption::VALUE_REQUIRED,
                        'Dictionary file path',
                        __DIR__ . '/../dictionary'
                    ),
                    new InputOption(
                        'passwords',
                        'p',
                        InputOption::VALUE_REQUIRED,
                        'Password file path',
                        __DIR__ . '/../passwords'
                    )
                ])
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Password bruteforce started!');
        $exitCode = 0;

        try {
            $bruteforcer = new Bruteforcer(
                new PasswordGenerator($input->getOption('dictionary')),
                new SaltGenerator(
                    2,
                    ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k']
                ),
                $input->getOption('passwords')
            );

            foreach ($bruteforcer->execute() as $decoded) {
                $output->writeln(
                    sprintf(
                        "Found [ hash: %s\t password: %s\t salt: %s\t count: %d\t seconds: %.2f ]",
                        $decoded['hash'],
                        $decoded['password'],
                        $decoded['salt'],
                        $decoded['count'],
                        $decoded['seconds']
                    )
                );
                // Print out only first cracked password.
                break;
            }
        } catch (FileException $e) {
            $output->writeln($e->getMessage());
            $exitCode = 1;
        } catch (\Exception $e) {
            $output->writeln($e->getMessage());
            $exitCode = 2;
        }

        $output->writeln('Password bruteforce finished!');
        return $exitCode;
    }
}
