<?php
namespace App\TestTask1\Helpers;


use App\TestTask1\Exceptions\FileException;

class FileReader
{
    /**
     * @param string $filePath
     * @return iterable
     * @throws FileException
     */
    public static function read(string $filePath): iterable
    {
        if (!file_exists($filePath)) {
            throw new FileException(
                "File not found! ($filePath)"
            );
        }

        $fileHandler = fopen($filePath, 'r');

        if ($fileHandler === false) {
            throw new FileException(
                "Can't read file! ($filePath)"
            );
        }

        try {
            while (!feof($fileHandler)) {
                yield trim(fgets($fileHandler));
            }
        } finally {
            fclose($fileHandler);
        }
    }
}
