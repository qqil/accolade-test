<?php
namespace App\TestTask1\Helpers;


class Hasher
{
    /**
     * @param $passwordHash
     * @param $password
     * @param string $algorithm
     * @return bool
     */
    public static function compare(
        $passwordHash,
        $password,
        $algorithm = 'md5'
    ): bool
    {
        return $passwordHash === hash($algorithm, $password);
    }
}
